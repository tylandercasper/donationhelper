package com.ibudesigns.donationhelper.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;
import java.io.InterruptedIOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ty on 6/8/2015.
 */
public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {
    ImageView bmImage;

    String url;

    public ImageDownloader(ImageView bmImage,String url) {
        this.bmImage = bmImage;
        this.url=url;
    }

    protected Bitmap doInBackground(String... urls) {

        Bitmap mIcon = null;
        try {
            InputStream in = new java.net.URL(url).openStream();
            mIcon = BitmapFactory.decodeStream(in);
        }catch (InterruptedIOException e){

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
        return mIcon;
    }

    public String getUrl(){
        return url;
    }

    protected void onPostExecute(Bitmap result) {
        bmImage.setImageBitmap(result);
        activeDownloads.remove(bmImage);
    }

    static Map<ImageView,ImageDownloader> activeDownloads=new HashMap<>();

    public static void LoadImage(ImageView view,String url)
    {
        ImageDownloader id;
        if(activeDownloads.containsKey(view))
        {
            id=activeDownloads.get(view);
            if(id.getUrl().equals(url)){
                return;
            }else{
                id.cancel(true);
            }

        }

        id=new ImageDownloader(view,url);
        activeDownloads.put(view,id);
        id.execute(url);
    }
}
