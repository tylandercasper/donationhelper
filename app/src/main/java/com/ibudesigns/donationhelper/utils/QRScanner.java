package com.ibudesigns.donationhelper.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Toast;

import com.ibudesigns.donationhelper.MainActivity;
import com.ibudesigns.donationhelper.contoller.DataManagers;
import com.ibudesigns.donationhelper.model.Donation;
import com.ibudesigns.donationhelper.view.donations.DonationDetials;

/**
 * Stolen from http://examples.javacodegeeks.com/android/android-barcode-and-qr-scanner-example/
 */
public class QRScanner implements ActivityResultListener{
    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";

    static final int SCAN_REQUEST_CODE=590;
    MainActivity mainActivity;

    public QRScanner(MainActivity activity){
        mainActivity=activity;
    }
    //product qr code mode
    public void scanQR() {
        try {
            //start the scanning activity from the com.google.zxing.client.android.SCAN intent
            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
            mainActivity.addActivityResultListener(this);
            mainActivity.startActivityForResult(intent, SCAN_REQUEST_CODE);
        } catch (ActivityNotFoundException anfe) {
            //on catch, show the download dialog
            showDialog(mainActivity, "No Scanner Found", "Download a scanner code activity?", "Yes", "No").show();
        }
    }

    //alert dialog for downloadDialog
    private static AlertDialog showDialog(final Activity act, CharSequence title, CharSequence message, CharSequence buttonYes, CharSequence buttonNo) {
        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    act.startActivity(intent);
                } catch (ActivityNotFoundException anfe) {

                }
            }
        });
        downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return downloadDialog.show();
    }

    //Todo seperate donation code from QRCode
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == SCAN_REQUEST_CODE) {
            mainActivity.removeActivityResultListener(this);
            if (resultCode == Activity.RESULT_OK) {
                //get the extras that are returned from the intent
                String contents = intent.getStringExtra("SCAN_RESULT");
                Donation donation=DataManagers.getDonationManager().getDonationFromQRCode(contents);
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                Toast toast = Toast.makeText(mainActivity, "Content:" + contents + " Format:" + format, Toast.LENGTH_LONG);
                toast.show();
                Intent i = new Intent(mainActivity, DonationDetials.class);
                i.putExtra("id", donation.Id);
                mainActivity.startActivity(i);

            }
        }
    }



}
