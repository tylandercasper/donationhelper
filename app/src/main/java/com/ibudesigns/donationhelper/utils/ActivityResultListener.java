package com.ibudesigns.donationhelper.utils;

import android.content.Intent;

/**
 * Created by Ty on 6/8/2015.
 */
public interface ActivityResultListener {
    public void onActivityResult(int requestCode, int resultCode, Intent intent);
}
