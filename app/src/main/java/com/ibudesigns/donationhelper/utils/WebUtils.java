package com.ibudesigns.donationhelper.utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by Ty on 6/9/2015.
 */
public class WebUtils {

    private static final String HTTPS = "https://";
    private static final String HTTP = "http://";

    public static void OpenURL(String url,Activity activity){
        if (!url.startsWith(HTTP) && !url.startsWith(HTTPS)) {
            url = HTTP + url;
        }

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        activity.startActivity(Intent.createChooser(intent, "Chose browser"));
    }
    public static void watchYoutubeVideo(String id,Activity activity){
        try{
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
            activity.startActivity(intent);
        }catch (ActivityNotFoundException ex){
            watchVideo("http://www.youtube.com/watch?v="+id,activity);
        }
    }

    public static void watchVideo(String url,Activity activity){
        Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        activity.startActivity(intent);
    }

}
