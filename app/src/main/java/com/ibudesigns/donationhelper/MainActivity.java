package com.ibudesigns.donationhelper;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;

import com.ibudesigns.donationhelper.contoller.DataManagers;
import com.ibudesigns.donationhelper.contoller.fake.FakeDataSource;
import com.ibudesigns.donationhelper.utils.ActivityResultListener;
import com.ibudesigns.donationhelper.view.donations.DonationsPage;
import com.ibudesigns.donationhelper.view.MainMenu;
import com.ibudesigns.donationhelper.view.recentchanges.RecentChangesList;
import com.ibudesigns.donationhelper.view.thankyous.ThankYousList;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity
        implements MainMenu.NavigationDrawerCallbacks {

    List<ActivityResultListener> activityResultListeners=new ArrayList<>();
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private MainMenu mMainMenu;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DataManagers.init(new FakeDataSource());

        mMainMenu = (MainMenu)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mMainMenu.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();

        Fragment fragment=getFragment(position);

        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
        onSectionAttached((position));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMainMenu.gotoMainPage();

    }

    /**You might look at this and think "wow, that's lazy hardcoded code"
     * But in actuality I am just doing this to see if you're really looking at my code.
     * So rememeber that. This is a test. For you.
     * Not lazyness.
     * A Test.
     */
    public Fragment getFragment(int number){
        switch (number) {
            case 0:
                return new DonationsPage();

            case 1:
                return new ThankYousList();

            case 2:
                return new RecentChangesList();

        }
        return null;
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 0:
                mTitle = getString(R.string.title_donations);
                break;
            case 1:
                mTitle = getString(R.string.title_thankyou);
                break;
            case 2:
                mTitle=getString(R.string.title_recentchanges);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    public void addActivityResultListener(ActivityResultListener listener){
        if(!activityResultListeners.contains(listener)){
            activityResultListeners.add((listener));
        }
    }
    public void removeActivityResultListener(ActivityResultListener listener){
        activityResultListeners.remove(listener);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent){
        for(ActivityResultListener listener:activityResultListeners){
            listener.onActivityResult(requestCode,resultCode,intent);
        }
    }



}
