package com.ibudesigns.donationhelper.contoller;

/**
 * Created by Ty on 6/3/2015.
 */
public interface IDataManagerSource {

    public IDonationManager getDonationManager();
    public IThankYouManager getThankYouManager();
    public IRecentChangeManager getRecentChangesManager();
}
