package com.ibudesigns.donationhelper.contoller;

/**
 * Created by Ty on 6/3/2015.
 *
 * Rudementary Factory class to manage the various sources
 * (this way when we change to real data we don't have to refactor our code to adjust)
 */
public class DataManagers {

    public static void init(IDataManagerSource managers){
        source=managers;
    }
    private static IDataManagerSource source;


    public static IRecentChangeManager getRecentChangesManager()
    {
        return source.getRecentChangesManager();
    }
    public static IThankYouManager getThankYouManager() {
        return source.getThankYouManager();
    }

    public static IDonationManager getDonationManager() {
        return source.getDonationManager();
    }

}
