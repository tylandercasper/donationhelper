package com.ibudesigns.donationhelper.contoller.fake;

import com.ibudesigns.donationhelper.contoller.IDataManagerSource;
import com.ibudesigns.donationhelper.contoller.IDonationManager;
import com.ibudesigns.donationhelper.contoller.IRecentChangeManager;
import com.ibudesigns.donationhelper.contoller.IThankYouManager;

/**
 * Created by Ty on 6/3/2015.
 */
public class FakeDataSource implements IDataManagerSource {

    private FakeDonationManager donationManager=new FakeDonationManager();
    private FakeThankYouManager thankYouManager=new FakeThankYouManager();
    private FakeRecentChangesManager recentChangesManager=new FakeRecentChangesManager();

    @Override
    public IDonationManager getDonationManager() {
        return donationManager;
    }

    @Override
    public IThankYouManager getThankYouManager() {
        return thankYouManager;
    }

    @Override
    public IRecentChangeManager getRecentChangesManager() {
        return recentChangesManager;
    }
}
