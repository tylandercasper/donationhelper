package com.ibudesigns.donationhelper.contoller;

import com.ibudesigns.donationhelper.model.RecentChange;

import java.util.List;

/**
 * Created by Ty on 6/8/2015.
 */
public interface IRecentChangeManager {

    public List<RecentChange> getRecentChanges();
}
