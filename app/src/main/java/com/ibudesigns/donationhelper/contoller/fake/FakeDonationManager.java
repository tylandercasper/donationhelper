package com.ibudesigns.donationhelper.contoller.fake;

import com.ibudesigns.donationhelper.contoller.DataManagers;
import com.ibudesigns.donationhelper.contoller.IDonationManager;
import com.ibudesigns.donationhelper.model.Donation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ty on 6/3/2015.
 */
public class FakeDonationManager implements IDonationManager {

    private List<Donation> donations=new ArrayList<Donation>();


    private Map<Integer,Donation> donationMappings=new HashMap<Integer,Donation>();
    public FakeDonationManager(){

        donations.add(new Donation(1,"Wikipedia","http://upload.wikimedia.org/wikipedia/en/2/28/WikipediaMobileAppLogo.png","http://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Wikipedia-logo-en-big.png/489px-Wikipedia-logo-en-big.png","http://en.wikipedia.org","An encylopdia of knowledge"));
        donations.add(new Donation(2,"XKCD","http://allaboutwindowsphone.com/images/appicons/96586.png","http://imgs.xkcd.com/static/terrible_small_logo.png","http://xkcd.com","A webcomic of romance,sarcasm, math, and language."));
        donations.add(new Donation(3,"Penny Arcade","http://wscont1.apps.microsoft.com/winstore/1x/0bbf2e1d-5659-474b-b187-a1f855600773/Icon.123366.png","http://vignette1.wikia.nocookie.net/playstationallstarsfanfictionroyale/images/5/51/Penny_arcade_logo.png/revision/latest?cb=20130526083636","http://penny-arcade.com","WebComic"));
        donations.add(new Donation(4,"Peter Hollins","https://yt3.ggpht.com/-AsNR-cbw9fU/AAAAAAAAAAI/AAAAAAAAAAA/awMB9F1zVDg/s100-c-k-no/photo.jpg","https://lh3.googleusercontent.com/-EsFRFXIX8MA/Tg480v-xyLI/AAAAAAAAJoE/zHlOKpKHRuI/w744-h745-no/Icon%2Bpermanent.jpg","https://www.youtube.com/user/peterhollens","My name is Peter Hollens and I make music using only my voice and mouth."));
        donations.add(new Donation(5,"Alex G","https://yt3.ggpht.com/-xmhaZ0lNLLs/AAAAAAAAAAI/AAAAAAAAAAA/Jfh9HWlvojQ/s100-c-k-no/photo.jpg","https://lh6.googleusercontent.com/-gajwng3_2Vw/VHZfBcUis9I/AAAAAAAAEJ4/CscpHAfk6Go/w832-h836-no/PICALEX.jpg","https://www.youtube.com/user/AlexGMusic7","Youtube Musician"));
        donations.add(new Donation(6,"Humane Society","http://www.humanesociety.org/assets/images/global/humane-society-united-states-logo-60th.png","http://www.humanesociety.org/assets/images/global/humane-society-united-states-logo-60th.png","http://www.humanesociety.org/","The nation's largest and most effective animal protection organization"));
        donations.add(new Donation(7,"Habitat For Humanity","https://lh6.googleusercontent.com/-VkPS0_4OatE/UToF8dhinOI/AAAAAAAAATY/nAb1ovXRWIg/s250-no/gplusicon-hfhi.png","https://lh6.googleusercontent.com/-VkPS0_4OatE/UToF8dhinOI/AAAAAAAAATY/nAb1ovXRWIg/s250-no/gplusicon-hfhi.png","http://www.habitat.org","Habitat for Humanity's vision is a world where everyone has a decent place to live."));

        for(Donation d : donations) {
            donationMappings.put(d.Id,d);
        }
    }


    @Override
    public List<Donation> getDonations() {

        return donations;
    }

    @Override
    public Donation getDonation(int id) {
        Donation d=donationMappings.get(id);


        return d;
    }

    @Override
    public Donation getDonationFromQRCode(String id) {
        return getDonation(4);
    }

    @Override
    public void Cancel(Donation donation) {
        if(donations.contains(donation)) {
            donations.remove(donation);
            ((FakeRecentChangesManager)DataManagers.getRecentChangesManager()).canceled(donation);
        }
    }

    @Override
    public void Subscribe(Donation donation)
    {
        if(!donations.contains(donation)) {
            donations.add(donation);
            ((FakeRecentChangesManager)DataManagers.getRecentChangesManager()).subscribed(donation);
        }
    }

    @Override
    public boolean isSubscribed(Donation donation) {
        return donations.contains(donation);
    }
}
