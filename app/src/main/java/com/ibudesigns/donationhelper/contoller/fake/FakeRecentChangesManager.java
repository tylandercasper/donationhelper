package com.ibudesigns.donationhelper.contoller.fake;

import com.ibudesigns.donationhelper.contoller.IRecentChangeManager;
import com.ibudesigns.donationhelper.model.Donation;
import com.ibudesigns.donationhelper.model.RecentChange;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ty on 6/8/2015.
 */
public class FakeRecentChangesManager implements IRecentChangeManager{

    private List<RecentChange> recentChanges=new ArrayList<RecentChange>();
    @Override
    public List<RecentChange> getRecentChanges() {
        return recentChanges;
    }

    public void canceled(Donation donation)
    {
        RecentChange change=new RecentChange();
        change.ChangeType=RecentChange.CANCEL;
        change.DonationId=donation.Id;
        recentChanges.add(change);

    }
    public void subscribed(Donation donation)
    {
        RecentChange change=new RecentChange();
        change.ChangeType=RecentChange.SUBSCRIBE;
        change.DonationId=donation.Id;
        recentChanges.add(change);
    }
}
