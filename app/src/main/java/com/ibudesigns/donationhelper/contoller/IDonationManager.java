package com.ibudesigns.donationhelper.contoller;

import com.ibudesigns.donationhelper.model.Donation;

import java.util.List;

/**
 * Created by Ty on 6/3/2015.
 */
public interface IDonationManager {
    public List<Donation> getDonations();
    public Donation getDonation(int id);
    public Donation getDonationFromQRCode(String code);
    public void Cancel(Donation donation);
    public void Subscribe(Donation donation);
    public boolean isSubscribed(Donation donation);
}
