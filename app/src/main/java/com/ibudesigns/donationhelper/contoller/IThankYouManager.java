package com.ibudesigns.donationhelper.contoller;

import com.ibudesigns.donationhelper.model.Donation;
import com.ibudesigns.donationhelper.model.ThankYou;

import java.util.List;

/**
 * Created by Ty on 6/3/2015.
 */
public interface IThankYouManager {

    public List<ThankYou> getThankYous();
    public ThankYou getThankYou(int id);
    public List<ThankYou> getThankYousForDonation(Donation donation);
}
