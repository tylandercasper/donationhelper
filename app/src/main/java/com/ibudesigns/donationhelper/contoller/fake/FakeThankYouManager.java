package com.ibudesigns.donationhelper.contoller.fake;

import com.ibudesigns.donationhelper.contoller.IThankYouManager;
import com.ibudesigns.donationhelper.model.Donation;
import com.ibudesigns.donationhelper.model.ThankYou;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ty on 6/3/2015.
 */
public class FakeThankYouManager implements IThankYouManager {

    List<ThankYou> thankYous=new ArrayList<ThankYou>();
    public FakeThankYouManager()
    {
        thankYous.add(new ThankYou(1,4,ThankYou.VIDEO,"rfPXZZEf7iQ"));


    }
    @Override
    public List<ThankYou> getThankYous() {
        return thankYous;
    }

    @Override
    public ThankYou getThankYou(int id) {
        for(ThankYou ty : thankYous){
            if(ty.Id==id){
                return ty;
            }
        }
        return null;
    }

    @Override
    public List<ThankYou> getThankYousForDonation(Donation donation) {
        List<ThankYou> donationThankYous=new ArrayList<>();
        for(ThankYou ty:thankYous){
            if(ty.DonationId==donation.Id){
                donationThankYous.add((ty));
            }
        }
        return donationThankYous;
    }
}
