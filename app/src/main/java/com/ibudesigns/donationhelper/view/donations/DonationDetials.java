package com.ibudesigns.donationhelper.view.donations;

import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibudesigns.donationhelper.R;
import com.ibudesigns.donationhelper.contoller.DataManagers;
import com.ibudesigns.donationhelper.model.Donation;
import com.ibudesigns.donationhelper.utils.ImageDownloader;
import com.ibudesigns.donationhelper.utils.WebUtils;

public class DonationDetials extends ActionBarActivity {

    Donation donation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.donation_details_page);

        Bundle b = getIntent().getExtras();
        int id = b.getInt("id");
        donation= DataManagers.getDonationManager().getDonation(id);

        TextView textViewItem = (TextView) findViewById(R.id.donationName);
        textViewItem.setText(donation.Name);

        textViewItem = (TextView) findViewById(R.id.donationDescription);
        textViewItem.setText(donation.About);

        Button url=(Button)findViewById(R.id.donationMainUrl);
        url.setText((donation.MainUrl));


        ImageView imageViewItem = (ImageView) findViewById(R.id.donationImage);
        ImageDownloader.LoadImage(imageViewItem, donation.ImageUrl);

        if(DataManagers.getDonationManager().isSubscribed(donation)){
            Button subscribe=(Button)findViewById(R.id.buttonSubscribe);
            subscribe.setVisibility(View.INVISIBLE);
        }else{
            Button subscribe=(Button)findViewById(R.id.buttonCancelSubscription);
            subscribe.setVisibility(View.INVISIBLE);
        }


    }

    public void openUrl(View v){

        WebUtils.OpenURL(donation.MainUrl,this);
    }

    public void showThankYous(View v){

    }
    public void subscribe(View v){
        DataManagers.getDonationManager().Subscribe(donation);
        finish();
    }

    public void cancel(View v) {
        DataManagers.getDonationManager().Cancel(donation);
        finish();

    }
}
