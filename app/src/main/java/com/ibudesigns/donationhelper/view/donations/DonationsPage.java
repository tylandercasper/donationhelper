package com.ibudesigns.donationhelper.view.donations;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.ibudesigns.donationhelper.MainActivity;
import com.ibudesigns.donationhelper.R;
import com.ibudesigns.donationhelper.contoller.DataManagers;
import com.ibudesigns.donationhelper.model.Donation;
import com.ibudesigns.donationhelper.utils.QRScanner;

/**
 * Created by Ty on 6/6/2015.
 */
public class DonationsPage extends Fragment {

    private ListView donationsList;
    Donation data[];

    View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.donations_page, container, false);



        Button button=(Button)rootView.findViewById(R.id.buttonQRScan);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanQR(v);
            }

        });


        donationsList = (ListView) rootView.findViewById(R.id.listView);
        donationsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItem(position);
            }
        });
        updateDonations();


        return rootView;
    }

    private void updateDonations(){

        data=new Donation[0];

        //Future versions we would do this asyncrounously, but for now we're being lazy
        data=DataManagers.getDonationManager().getDonations().toArray(data);

        TextView salesText=(TextView)rootView.findViewById(R.id.salesPitchText);
        String sales=getString(R.string.salesInfo);
        sales=sales.replace("$$","$"+data.length);
        salesText.setText(sales);

        DonationsAdapter adapter = new DonationsAdapter(getActivity(), R.layout.donations_list_view_item, data);
        donationsList.setAdapter(adapter);
    }

    private void selectItem(int position)
    {
        Donation donation=data[position];

        Intent i = new Intent(this.getActivity(), DonationDetials.class);
        i.putExtra("id", donation.Id);
        startActivity(i);


    }

    public void scanQR(View v)
    {
        MainActivity activity=(MainActivity)getActivity();
        QRScanner scanner=new QRScanner(activity);
        scanner.scanQR();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateDonations();
    }
}
