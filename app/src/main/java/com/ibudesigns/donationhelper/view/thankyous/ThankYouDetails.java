package com.ibudesigns.donationhelper.view.thankyous;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibudesigns.donationhelper.R;
import com.ibudesigns.donationhelper.contoller.DataManagers;
import com.ibudesigns.donationhelper.model.Donation;
import com.ibudesigns.donationhelper.model.ThankYou;
import com.ibudesigns.donationhelper.utils.WebUtils;

public class ThankYouDetails extends ActionBarActivity {

    ThankYou thankYou;
    Donation donation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thankyou_details_page);


        Bundle b = getIntent().getExtras();
        int id = b.getInt("id");

        thankYou=DataManagers.getThankYouManager().getThankYou(id);
        donation= DataManagers.getDonationManager().getDonation(thankYou.DonationId);

    }



    public void watchVideo(View v){

        if(thankYou.URL.startsWith("http")){
            WebUtils.watchVideo((thankYou.URL), this);
        }else{
            WebUtils.watchYoutubeVideo(thankYou.URL,this);
        }
    }
}
