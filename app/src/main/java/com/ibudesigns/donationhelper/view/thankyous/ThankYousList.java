package com.ibudesigns.donationhelper.view.thankyous;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ibudesigns.donationhelper.R;
import com.ibudesigns.donationhelper.contoller.DataManagers;
import com.ibudesigns.donationhelper.model.ThankYou;

/**
 * Created by Ty on 6/6/2015.
 */
public class ThankYousList extends Fragment
{
    private ListView thankYousList;
    ThankYou data[] = null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.thankyous_list_page, container, false);

        thankYousList = (ListView) rootView.findViewById(R.id.listView);
        thankYousList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItem(position);
            }
        });

        data=new ThankYou[0];

        //Future versions we would do this asyncrounously, but for now we're being lazy
        data= DataManagers.getThankYouManager().getThankYous().toArray(data);

        ThankYousAdapter adapter = new ThankYousAdapter(getActivity(), R.layout.thankyous_list_view_item, data);
        thankYousList.setAdapter(adapter);

        return rootView;
    }

    private void selectItem(int position)
    {
        ThankYou thankYou=data[position];

        Intent i = new Intent(this.getActivity(), ThankYouDetails.class);
        i.putExtra("id", thankYou.Id);
        startActivity(i);


    }
}
