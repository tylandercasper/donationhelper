package com.ibudesigns.donationhelper.view.recentchanges;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ibudesigns.donationhelper.R;
import com.ibudesigns.donationhelper.contoller.DataManagers;
import com.ibudesigns.donationhelper.model.RecentChange;
import com.ibudesigns.donationhelper.model.ThankYou;
import com.ibudesigns.donationhelper.view.donations.DonationDetials;
import com.ibudesigns.donationhelper.view.thankyous.ThankYouDetails;
import com.ibudesigns.donationhelper.view.thankyous.ThankYousAdapter;

/**
 * Created by Ty on 6/6/2015.
 */
public class RecentChangesList extends Fragment
{
    private ListView recentChangesList;
    RecentChange data[] = null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.recentchanges_list_page, container, false);

        recentChangesList = (ListView) rootView.findViewById(R.id.listView);
        recentChangesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItem(position);
            }
        });

        data=new RecentChange[0];

        //Future versions we would do this asyncrounously, but for now we're being lazy
        data= DataManagers.getRecentChangesManager().getRecentChanges().toArray(data);

        ChangesAdapter adapter = new ChangesAdapter(getActivity(), R.layout.donations_list_view_item, data);
        recentChangesList.setAdapter(adapter);

        return rootView;
    }

    private void selectItem(int position)
    {
        RecentChange change=data[position];

        Intent i = new Intent(this.getActivity(), DonationDetials.class);
        i.putExtra("id", change.DonationId);
        startActivity(i);


    }
}
