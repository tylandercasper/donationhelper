package com.ibudesigns.donationhelper.view.recentchanges;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibudesigns.donationhelper.R;
import com.ibudesigns.donationhelper.contoller.DataManagers;
import com.ibudesigns.donationhelper.model.Donation;
import com.ibudesigns.donationhelper.model.RecentChange;
import com.ibudesigns.donationhelper.utils.ImageDownloader;

/**
 * Created by Ty on 6/6/2015.
 */
public class ChangesAdapter extends ArrayAdapter<RecentChange> {


    Context mContext;
    int layoutResourceId;
    RecentChange data[] = null;

    public ChangesAdapter(Context mContext, int layoutResourceId, RecentChange[] data) {

        super(mContext, layoutResourceId, data);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }

        // object item based on the position
        RecentChange change=data[position];
        Donation donation = DataManagers.getDonationManager().getDonation(change.DonationId);

        // get the TextView and then set the text (item name) and tag (item ID) values
        TextView textViewItem = (TextView) convertView.findViewById(R.id.donationName);
        String beginning="";
        switch(change.ChangeType){
            case RecentChange.CANCEL:
                beginning=mContext.getString(R.string.ChangeCanceled);
                break;
            case RecentChange.SUBSCRIBE:
                beginning=mContext.getString(R.string.ChangeSubscribed);
                break;
        }
        beginning+=": ";
        textViewItem.setText(beginning+donation.Name);

        ImageView imageViewItem = (ImageView) convertView.findViewById(R.id.donationIcon);
        ImageDownloader.LoadImage(imageViewItem, donation.IconUrl);


        return convertView;

    }
}
