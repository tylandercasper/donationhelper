package com.ibudesigns.donationhelper.model;

/**
 * Created by Ty on 6/3/2015.
 */
public class Donation {

    public Donation(){

    }
    public Donation(int id,String name,String icon,String image,String url,String about){
        Id=id;
        Name=name;
        IconUrl=icon;
        ImageUrl=image;
        MainUrl=url;
        About=about;
    }
    public String Name;
    public String IconUrl;
    public String ImageUrl;
    public String MainUrl;
    public String About;
    public int Id;
}
