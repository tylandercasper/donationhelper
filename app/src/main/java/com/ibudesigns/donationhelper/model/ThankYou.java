package com.ibudesigns.donationhelper.model;

/**
 * Created by Ty on 6/3/2015.
 */
public class ThankYou {
    public static final int VIDEO=1;
    public static final int IMAGE=2;
    public static final int TEXT=3;

    public ThankYou(int id,int donationId,int type,String url){
        Id=id;
        DonationId=donationId;
        Type=type;
        URL=url;
    }



    public int Id;
    public int DonationId;
    public String URL;
    public String Text;
    public int Type;

}
