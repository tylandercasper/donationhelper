About the App:

The app is a utility to work with another side project I'm doing that allows people to donate to anyone. The idea is the easier we make it to donate, the more people will do it, so this project is about letting people donate a dollar a month to support their favorite sites, artists and causes in single consistent place. (That way its just a click of a button to help out.)

The app itself assists the user by letting them manage their donations, add new donations by scanning in QR codes (I'm picturing charities), and also lets them receive thank you videos from people they're supporting. Its a "feel good about the good I'm doing" app, and its designed to make them feel better and know that they're appreciated.


About the feature set:

I feel like for mobile especially, simplicity is best. I think its better to do a few things really well than a lot of things poorly or even mediocrely. So in that regard I dont think there's much else I would do to this. Thank You notifications is really the only core functionality that I would definetely add, and possibly a way to change how much you're donating to a single person. (Charities might need more than a dollar a month for example)
But overall I'm very happy with the functionality this app has.



About the Code:

I was going to take some time to clean up the code a little bit more, but in the interest of representation, I thought it would be better to keep it at the 10 hour mark and talk about how I spent my time:

I think I spent about 5 hours on the flow and feature representation. Basically "What will the app do and how will it do it?" Its a lot of time for sure, but I think its really important to focus on getting the core functionality to work well rather than just jump in gung ho without a clean idea of what you want to accomplish.

As far as the code itself goes, the project was not actually very technically challenging so I don't know how representative it is of my mad coding skills, but I think there's 3 main things worth focusing on:

The first is to acknowledge that the menu code is indeed atrocious. It was the default code for the android menu template and I was surprised at how poorly engineered it was (especially for something that so many people will be using). We always have a choice to refactor or work with the bad code, and in the interest of time I went with the bad code. (A pick your battles sort of thing). In the future I would definitely refactor it to be more clean.

The second thing is my DataManagers class. Because for this project I'm using fake data and in the future I will want to use real data, I made this class which is a very simple factory pattern. Everything is an interface and the managers can be easily swapped out.None of the views care about where its getting its data and the managers don't care whats being done with it, and its very easy to swap out the old managers for new managers.

The third thing is a basic event handler for the "onActivityResult" code. I  don't like coupling my root activity with specific functionality from other classes, so instead I added an event handler so other classes can subscribe and handle it as they see fit.


About the Future:
Things that need to be done are:
Better image caching
asynchrous data calls
Menu cleanup.
